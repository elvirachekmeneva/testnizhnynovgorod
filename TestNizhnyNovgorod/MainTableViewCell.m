//
//  MainTableViewCell.m
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "MainTableViewCell.h"
#define ImageURL( okved ) ([NSString stringWithFormat: @"http://basis.seldon.ru/Content/img/okved-sub-categories/112/%@.png", okved])

@implementation MainTableViewCell

- (void)awakeFromNib {
    // Initialization code
    gradient = [CAGradientLayer layer];
    gradient.frame = self.shadowView.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    gradient.opacity = .15;
    [self.shadowView.layer insertSublayer:gradient atIndex:0];
    self.shadowView.layer.masksToBounds = YES;
    self.layer.borderColor = [UIColor grayColor].CGColor;
    self.layer.borderWidth = .5;
    self.layer.cornerRadius = 5;
    
    self.roundView.layer.cornerRadius = 35;

    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setData:(NSDictionary *)data {
    NSString* colorString = [data[@"color"] stringByReplacingOccurrencesOfString:@"#" withString:@""];
    self.roundView.backgroundColor = [UIColor colorWithHexString:colorString];
    self.shortNameLbl.text = data[@"shortName"];
    self.addressLbl.text = data [@"address"];
    self.okvedId = data[@"okvedId"];
}

- (void)setOkvedId:(NSString *)okvedId {
    _okvedId = okvedId;
    [self setImageToImageView:self.okvedImageView withURL:ImageURL(okvedId)];
}

- (void)setImageToImageView:(UIImageView*)imgView withURL:(NSString*)URL {
    
    UIImage* img = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:URL];
    if( img ){
        [imgView setImage:img];
         NSLog(@"%@", imgView);
        
    } else {
        [imgView sd_setImageWithURL:[NSURL URLWithString:URL] placeholderImage:nil completed:nil];
    }
}
@end
