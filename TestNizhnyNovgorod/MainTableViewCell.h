//
//  MainTableViewCell.h
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HexColors.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface MainTableViewCell : UITableViewCell {
    CAGradientLayer *gradient;
}

@property (nonatomic, strong) NSDictionary* data;
@property (nonatomic, strong) NSString* okvedId;

@property (strong, nonatomic) IBOutlet UIView *roundView;
@property (strong, nonatomic) IBOutlet UIImageView *okvedImageView;
@property (strong, nonatomic) IBOutlet UILabel *shortNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *addressLbl;
@property (strong, nonatomic) IBOutlet UIView *shadowView;


@end
