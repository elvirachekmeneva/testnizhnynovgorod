//
//  MainTableViewController.m
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "MainTableViewController.h"

@interface MainTableViewController ()

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [[DataMgr sharedMgr] addObserver:self forKeyPath:@"companies" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial) context:nil];
    self.tableView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0);
    [DataMgr sharedMgr].delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"companies"]) {
        [self.tableView reloadData];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [DataMgr sharedMgr].companies.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.data = [DataMgr sharedMgr].companies[indexPath.row];
    
    return cell;
}

- (void)internetConnectionError:(NSString *)errorText {
    UIAlertView* internetErrorAlert = [[UIAlertView alloc] initWithTitle:@"Internet error" message:errorText delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [internetErrorAlert show];
}

- (void)serverError:(NSString *)erroText {
    UIAlertView* internetErrorAlert = [[UIAlertView alloc] initWithTitle:@"Server error" message:erroText delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [internetErrorAlert show];
}

@end
