//
//  MainTableViewController.h
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainTableViewCell.h"
#import "DataMgr.h"

@interface MainTableViewController : UITableViewController <DataMgrDelegate>

@end
