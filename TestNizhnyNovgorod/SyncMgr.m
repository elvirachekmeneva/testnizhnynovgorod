//
//  SyncMgr.m
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "SyncMgr.h"

@implementation SyncMgr

+ (SyncMgr*)sharedMgr {
    static SyncMgr *sharedMyMgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyMgr = [[self alloc] init];
    });
    return sharedMyMgr;
}

- (id)init {
    if (self = [super init]) {
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
            if (status == AFNetworkReachabilityStatusReachableViaWWAN ||
                status ==AFNetworkReachabilityStatusReachableViaWiFi) {
                if (errorHapen) {
                    errorHapen = NO;
                    [self.delegate connected];
                }
            }
        }];
        errorHapen = NO;
    }
    return self;
}

- (void) updateDataForPageIndex:(NSInteger) index pageSize:(NSInteger)size block:(void (^)(NSArray* data, NSError *error))block{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary* parameters = @{@"searchstring":@"",
                                 @"pageindex" : @(index),
                                 @"pagesize" : @(size)};
    [manager POST:@"http://basis.seldon.ru/companies/search" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if (block) {
            if (responseObject[@"response"][@"items"]) {
               
                block(responseObject[@"response"][@"items"], nil);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        errorHapen = YES;
        if (block) {
            block(nil, error);
        }
    }];

}




@end
