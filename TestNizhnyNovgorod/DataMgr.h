//
//  DataMgr.h
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SyncMgr.h"

@protocol DataMgrDelegate <NSObject>

@optional
- (void) internetConnectionError:(NSString*) errorText;
- (void) serverError:(NSString*) erroText;

@end

@interface DataMgr : NSObject <SyncMgrDelegate>

@property (nonatomic, strong) NSArray* companies;

@property (weak, nonatomic) id<DataMgrDelegate> delegate;

+ (DataMgr *)sharedMgr;

@end
