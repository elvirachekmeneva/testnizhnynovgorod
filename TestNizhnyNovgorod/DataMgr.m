//
//  DataMgr.m
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import "DataMgr.h"

@implementation DataMgr

+ (DataMgr *)sharedMgr {
    static DataMgr *sharedMyMgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyMgr = [[self alloc] init];
    });
    return sharedMyMgr;
}

- (id)init {
    if (self = [super init]) {
        [SyncMgr sharedMgr].delegate = self;
    }
    return self;
}

- (NSArray *)companies {
    if (!_companies) {
        _companies = @[];
        [self update];
        
    }
    return  _companies;
}

- (void) update {
    [[SyncMgr sharedMgr] updateDataForPageIndex:1 pageSize:20 block:^(NSArray *data, NSError *error) {
        if (!error) {
            self.companies = data;
        } else {
            
            NSInteger statusCode = error.code;
            if (statusCode == -1009 || statusCode == -1004) {
                [self.delegate internetConnectionError:error.description];
            } else {
                [self.delegate serverError:error.description];
            }
            
        }
    }];
}

- (void)connected {
    [self update];
}

@end
