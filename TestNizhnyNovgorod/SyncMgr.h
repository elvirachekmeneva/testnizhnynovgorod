//
//  SyncMgr.h
//  TestNizhnyNovgorod
//
//  Created by Эльвира on 19.02.15.
//  Copyright (c) 2015 Эльвира. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "AFNetworkReachabilityManager.h"

@protocol SyncMgrDelegate <NSObject>

@optional
- (void) connected;

@end

@interface SyncMgr : NSObject{
    BOOL errorHapen;
}
@property (weak, nonatomic) id<SyncMgrDelegate> delegate;


+ (SyncMgr*)sharedMgr;
- (void) updateDataForPageIndex:(NSInteger)index pageSize:(NSInteger)size block:(void (^)(NSArray* data, NSError *error))block;
@end
